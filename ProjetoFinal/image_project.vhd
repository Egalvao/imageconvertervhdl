library STD;
use STD.textio.all;                     -- basic I/O

library IEEE;
use IEEE.std_logic_1164.all;            -- basic logic types
use IEEE.std_logic_textio.all;          -- I/O for logic types
use IEEE.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

Library sim_util;


entity image_project is
	-- Gravar Arquivo de Log.
	generic(
		log_file : string := "log_image.log"
	);

	port(
		-- Resolução da Imagem Altura X Largura
		Pic_height:  integer := 2;
		Pic_width:  integer := 2;
		-- Tamanho Da imagem em blocos de Array
		Size_Pic: integer := (Pic_height*pic_width)
		);
		
end image_project;

architecture converter of image_project is
	
		-- Array da Simulação de Imagem.
		variable ex_Array_Image : 	std_logic_vector(0 to Size_Pic);
		variable GS_Array_Image : 	std_logic_vector(0 to Size_Pic);
		variable R_Array : std_logic_vector(0 to Size_Pic); 
		variable G_Array : std_logic_vector(0 to Size_Pic); 
		variable B_Array : std_logic_vector(0 to Size_Pic); 
		
		-- Constantes de Tipação de arquivo
		constant r : real := 0.299; 
		constant g : real := 0.587;
		constant b : real := 0.114;
		
		-- Variavel para Gravar o Log
		File l_file: TEXT open write_mode is log_file;
		
		-- Passando os Argumentos
		begin
		process (Size_Pic)
		
begin

	for cont in 0 to Size_Pic loop
		-- Calculo de RGB to GrayScale -> 0.299 * vermelho + 0,587 * verde + 0.114 * azul 
		-- GS_Array_Image(cont) := (unsigned(R_Array(cont)* r) + unsigned(G_Array(cont)* g) + unsigned(B_Array(cont)* b));
		GS_Array_Image(cont) := R_array(cont);
	end loop;
	
end process;
end converter;