-------------------------------------------------------------------------------
-- FILE NAME   : SIM_UTIL.VHD
-- PROJECT     : 
-- VERSION     : A
-- REVISION    : A
-- AUTHOR      : Stefan Doll
-- SIM/SYN     : Simulation
-- DESCRIPTION : Library of simulation utilities
-- DEPENDENCIES: IEEE.STD_LOGIC_1164
-- LIMITATIONS :
-------------------------------------------------------------------------------



-- currently 2 packages in this library - txt_util & conv_util

-------------------------------------------------------------------------------
-- CHANGE HISTORY.
-- REV.           DATE        DESCRIPTION          
--  A             02/08/99    Initial design.
-------------------------------------------------------------------------------




-- -------------------------------------------------------------------
--    Package for VHDL text output
--
--     This package uses the VHDL 95 standard. If VHDL 95 is not supported, 
--     comment out the file access functions.
--
--    The package provides a means to output text and manipulate strings. 
--
--    The basic usage is like this: >> print(s); <<
--    (where s is any string)
--    To print something which is not a string it has to be converted
--    into a string first. For this purpose the package contains
--    conversion functions called >> str(...) <<.
--    For example a std_logic_vector slv would be printed like this:
--    >> print(str(slv)); <<. To print several items on one line the
--    items have to concatenated as strings with the "&" operator eg:
--    >> print("The value of slv is "& str(slv)); <<
--    The string functions can also be used in assert statements as shown
--    in the example below:
--    >> assert DIN = "0101"  <<
--    >>   report "DIN = "& str(DIN)& " expected 0101 "  <<
--    >>   severity Error;  <<
-- -------------------------------------------------------------------


------------------------------------------------------------------------------
-- MODIFICATIONS:
--
-- DATE      AUTHOR            CHANGE
-- 2/8/99    M. S. HARVEY      Added printt procedure - prints string to file, 
--                             appends current sim time.
--
------------------------------------------------------------------------------


LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE std.textio.ALL;


PACKAGE txt_util IS
  
  -- prints a message to the screen
  PROCEDURE print(text: string);
  
  -- prints the message when active
  -- useful for debug switches
  PROCEDURE print(active: boolean; text: string);
  
  -- converts std_logic into a character
  FUNCTION chr(sl: std_logic) RETURN character;
  
  -- converts std_logic into a string (1 to 1)
  FUNCTION str(sl: std_logic) RETURN string;
  
  -- converts std_logic_vector into a string (binary base)
  FUNCTION str(slv: std_logic_vector) RETURN string;
  
  -- converts boolean into a string
  FUNCTION str(b: boolean) RETURN string;
  
  -- converts an integer into a single character
  -- (can also be used for hex conversion and other bases)
  FUNCTION chr(int: integer) RETURN character;
  
  -- converts integer into string using specified base
  FUNCTION str(int: integer; base: integer) RETURN string;
  
  -- converts integer to string, using base 10
  FUNCTION str(int: integer) RETURN string;
  
  -- convert std_logic_vector into a string in hex format
  FUNCTION hstr(slv: std_logic_vector) RETURN string;
  
  
  -- functions to manipulate strings
  -----------------------------------
  
  -- convert a character to upper case
  FUNCTION to_upper(c: character) RETURN character;
  
  -- convert a character to lower case
  FUNCTION to_lower(c: character) RETURN character;
  
  -- convert a string to upper case
  FUNCTION to_upper(s: string) RETURN string;
  
  -- convert a string to lower case
  FUNCTION to_lower(s: string) RETURN string;
  
  
  
  -- functions to convert strings into other formats
  --------------------------------------------------
  
  -- converts a character into std_logic
  FUNCTION to_std_logic(c: character) RETURN std_logic; 
  
  -- converts a string into std_logic_vector
  FUNCTION to_std_logic_vector(s: string) RETURN std_logic_vector; 
  
  
  
  -- file I/O
  -----------
  
  -- read variable length string from input file
  PROCEDURE str_read(FILE in_file: TEXT; 
                       res_string: OUT string);
  
  -- print string to a file and start new line
  PROCEDURE print(FILE out_file: TEXT;
                     new_string: IN  string);
  
  -- print string to a file if active is true and start new line
  PROCEDURE print(        active: boolean;
                  FILE out_file : TEXT;
                     new_string : IN  string);
  
  -- print string to a file, append current sim time and start new line
  PROCEDURE printt(FILE out_file: TEXT;
                     new_string : IN  string);
  
  -- print character to a file and start new line
  PROCEDURE print(FILE out_file : TEXT;
                           char : IN  character);
  
  
END txt_util;




PACKAGE BODY txt_util IS
  
  
  
  
  -- prints text to the screen
  
  PROCEDURE print(text: string) IS
  VARIABLE msg_line: line;
  BEGIN
    write(msg_line, text);
    writeline(output, msg_line);
  END print;
  
  
  
  
  -- prints text to the screen when active
  
  PROCEDURE print(active: boolean; text: string)  IS
  BEGIN
    IF active THEN
      print(text);
    END IF;
  END print;
  
  
  -- converts std_logic into a character
  
  FUNCTION chr(sl: std_logic) RETURN character IS
  VARIABLE c: character;
  BEGIN
    CASE sl IS
      WHEN 'U' => c:= 'U';
      WHEN 'X' => c:= 'X';
      WHEN '0' => c:= '0';
      WHEN '1' => c:= '1';
      WHEN 'Z' => c:= 'Z';
      WHEN 'W' => c:= 'W';
      WHEN 'L' => c:= 'L';
      WHEN 'H' => c:= 'H';
      WHEN '-' => c:= '-';
    END CASE;
    RETURN c;
  END chr;
  
  
  
  -- converts std_logic into a string (1 to 1)
  
  FUNCTION str(sl: std_logic) RETURN string IS
  VARIABLE s: string(1 TO 1);
  BEGIN
    s(1) := chr(sl);
    RETURN s;
  END str;
  
  
  
  -- converts std_logic_vector into a string (binary base)
  -- (this also takes care of the fact that the range of
  --  a string is natural while a std_logic_vector may
  --  have an integer range)
  
  FUNCTION str(slv: std_logic_vector) RETURN string IS
  VARIABLE result : string (1 TO slv'length);
  VARIABLE r : integer;
  BEGIN
    r := 1;
    FOR i IN slv'RANGE LOOP
      result(r) := chr(slv(i));
      r := r + 1;
    END LOOP;
    RETURN result;
  END str;
  
  
  FUNCTION str(b: boolean) RETURN string IS
  
  BEGIN
    IF b THEN
      RETURN "true";
    ELSE
      RETURN "false";
    END IF;
  END str;
  
  
  -- converts an integer into a character
  -- for 0 to 9 the obvious mapping is used, higher
  -- values are mapped to the characters A-Z
  -- (this is usefull for systems with base > 10)
 
  FUNCTION chr(int: integer) RETURN character IS
  VARIABLE c: character;
  BEGIN
    CASE int IS
      WHEN  0 => c := '0';
      WHEN  1 => c := '1';
      WHEN  2 => c := '2';
      WHEN  3 => c := '3';
      WHEN  4 => c := '4';
      WHEN  5 => c := '5';
      WHEN  6 => c := '6';
      WHEN  7 => c := '7';
      WHEN  8 => c := '8';
      WHEN  9 => c := '9';
      WHEN 10 => c := 'A';
      WHEN 11 => c := 'B';
      WHEN 12 => c := 'C';
      WHEN 13 => c := 'D';
      WHEN 14 => c := 'E';
      WHEN 15 => c := 'F';
      WHEN 16 => c := 'G';
      WHEN 17 => c := 'H';
      WHEN 18 => c := 'I';
      WHEN 19 => c := 'J';
      WHEN 20 => c := 'K';
      WHEN 21 => c := 'L';
      WHEN 22 => c := 'M';
      WHEN 23 => c := 'N';
      WHEN 24 => c := 'O';
      WHEN 25 => c := 'P';
      WHEN 26 => c := 'Q';
      WHEN 27 => c := 'R';
      WHEN 28 => c := 'S';
      WHEN 29 => c := 'T';
      WHEN 30 => c := 'U';
      WHEN 31 => c := 'V';
      WHEN 32 => c := 'W';
      WHEN 33 => c := 'X';
      WHEN 34 => c := 'Y';
      WHEN 35 => c := 'Z';
      WHEN OTHERS => c := '?';
    END CASE;
    RETURN c;
  END chr;
  
  
  
  -- convert integer to string using specified base
  
  FUNCTION str(int: integer; base: integer) RETURN string IS
  
  VARIABLE temp:      string(1 TO 10);
  VARIABLE num:       integer;
  VARIABLE abs_int:   integer;
  VARIABLE len:       integer := 1;
  VARIABLE power:     integer := 1;
  
  BEGIN
    
    -- bug fix for negative numbers
    abs_int := ABS(int);
    
    num     := abs_int;
    
    WHILE num >= base LOOP                     -- Determine how many
      len := len + 1;                          -- characters required
      num := num / base;                       -- to represent the
    END LOOP ;                                 -- number.
    
    FOR i IN len DOWNTO 1 LOOP                 -- Convert the number to
      temp(i) := chr(abs_int/power MOD base);  -- a string starting
      power := power * base;                   -- with the right hand
    END LOOP ;                                 -- side.
    
    -- return result and add sign if required
    IF int < 0 THEN
      RETURN '-'& temp(1 TO len);
    ELSE
      RETURN temp(1 TO len);
    END IF;
    
  END str;
  
  
  -- convert integer to string, using base 10
  FUNCTION str(int: integer) RETURN string IS
  
  BEGIN
    
    RETURN str(int, 10) ;
    
  END str;
  
  
  
  -- converts a std_logic_vector into a hex string.
  FUNCTION hstr(slv: std_logic_vector) RETURN string IS
  VARIABLE hexlen: integer;
  VARIABLE longslv : std_logic_vector(67 DOWNTO 0) := (OTHERS => '0');
  VARIABLE hex : string(1 TO 16);
  VARIABLE fourbit : std_logic_vector(3 DOWNTO 0);
  BEGIN
    hexlen := (slv'left+1)/4;
    IF (slv'left+1) MOD 4 /= 0 THEN
      hexlen := hexlen + 1;
    END IF;
    longslv(slv'left DOWNTO 0) := slv;
    FOR i IN (hexlen -1) DOWNTO 0 LOOP
      fourbit := longslv(((i*4)+3) DOWNTO (i*4));
      CASE fourbit IS
        WHEN "0000" => hex(hexlen -I) := '0';
        WHEN "0001" => hex(hexlen -I) := '1';
        WHEN "0010" => hex(hexlen -I) := '2';
        WHEN "0011" => hex(hexlen -I) := '3';
        WHEN "0100" => hex(hexlen -I) := '4';
        WHEN "0101" => hex(hexlen -I) := '5';
        WHEN "0110" => hex(hexlen -I) := '6';
        WHEN "0111" => hex(hexlen -I) := '7';
        WHEN "1000" => hex(hexlen -I) := '8';
        WHEN "1001" => hex(hexlen -I) := '9';
        WHEN "1010" => hex(hexlen -I) := 'A';
        WHEN "1011" => hex(hexlen -I) := 'B';
        WHEN "1100" => hex(hexlen -I) := 'C';
        WHEN "1101" => hex(hexlen -I) := 'D';
        WHEN "1110" => hex(hexlen -I) := 'E';
        WHEN "1111" => hex(hexlen -I) := 'F';
        WHEN "ZZZZ" => hex(hexlen -I) := 'z';
        WHEN "UUUU" => hex(hexlen -I) := 'u';
        WHEN "XXXX" => hex(hexlen -I) := 'x';
        WHEN OTHERS => hex(hexlen -I) := '?';
      END CASE;
    END LOOP;
    RETURN hex(1 TO hexlen);
  END hstr;
  
  
  
  -- functions to manipulate strings
  -----------------------------------
  
  
  -- convert a character to upper case
  
  FUNCTION to_upper(c: character) RETURN character IS
  
  VARIABLE u: character;
  
  BEGIN
    
    CASE c IS
      WHEN 'a' => u := 'A';
      WHEN 'b' => u := 'B';
      WHEN 'c' => u := 'C';
      WHEN 'd' => u := 'D';
      WHEN 'e' => u := 'E';
      WHEN 'f' => u := 'F';
      WHEN 'g' => u := 'G';
      WHEN 'h' => u := 'H';
      WHEN 'i' => u := 'I';
      WHEN 'j' => u := 'J';
      WHEN 'k' => u := 'K';
      WHEN 'l' => u := 'L';
      WHEN 'm' => u := 'M';
      WHEN 'n' => u := 'N';
      WHEN 'o' => u := 'O';
      WHEN 'p' => u := 'P';
      WHEN 'q' => u := 'Q';
      WHEN 'r' => u := 'R';
      WHEN 's' => u := 'S';
      WHEN 't' => u := 'T';
      WHEN 'u' => u := 'U';
      WHEN 'v' => u := 'V';
      WHEN 'w' => u := 'W';
      WHEN 'x' => u := 'X';
      WHEN 'y' => u := 'Y';
      WHEN 'z' => u := 'Z';
      WHEN OTHERS => u := c;
    END CASE;
    
    RETURN u;
    
  END to_upper;
  
  
  -- convert a character to lower case
  
  FUNCTION to_lower(c: character) RETURN character IS
  
  VARIABLE l: character;
  
  BEGIN
    
    CASE c IS
      WHEN 'A' => l := 'a';
      WHEN 'B' => l := 'b';
      WHEN 'C' => l := 'c';
      WHEN 'D' => l := 'd';
      WHEN 'E' => l := 'e';
      WHEN 'F' => l := 'f';
      WHEN 'G' => l := 'g';
      WHEN 'H' => l := 'h';
      WHEN 'I' => l := 'i';
      WHEN 'J' => l := 'j';
      WHEN 'K' => l := 'k';
      WHEN 'L' => l := 'l';
      WHEN 'M' => l := 'm';
      WHEN 'N' => l := 'n';
      WHEN 'O' => l := 'o';
      WHEN 'P' => l := 'p';
      WHEN 'Q' => l := 'q';
      WHEN 'R' => l := 'r';
      WHEN 'S' => l := 's';
      WHEN 'T' => l := 't';
      WHEN 'U' => l := 'u';
      WHEN 'V' => l := 'v';
      WHEN 'W' => l := 'w';
      WHEN 'X' => l := 'x';
      WHEN 'Y' => l := 'y';
      WHEN 'Z' => l := 'z';
      WHEN OTHERS => l := c;
    END CASE;
    
    RETURN l;
    
  END to_lower;
  
  
  
  -- convert a string to upper case
  
  FUNCTION to_upper(s: string) RETURN string IS
  
  VARIABLE uppercase: string (s'RANGE);
  
  BEGIN
    
    FOR i IN s'RANGE LOOP
      uppercase(i):= to_upper(s(i));
    END LOOP;
    RETURN uppercase;
    
  END to_upper;
  
  
  
  -- convert a string to lower case
  
  FUNCTION to_lower(s: string) RETURN string IS
  
  VARIABLE lowercase: string (s'RANGE);
  
  BEGIN
    
    FOR i IN s'RANGE LOOP
      lowercase(i):= to_lower(s(i));
    END LOOP;
    RETURN lowercase;
    
  END to_lower;
  
  
  
  -- functions to convert strings into other types
  
  
  -- converts a character into a std_logic
  
  FUNCTION to_std_logic(c: character) RETURN std_logic IS 
  VARIABLE sl: std_logic;
  BEGIN
    CASE c IS
      WHEN 'U' => 
        sl := 'U'; 
      WHEN 'X' =>
        sl := 'X';
      WHEN '0' => 
        sl := '0';
      WHEN '1' => 
        sl := '1';
      WHEN 'Z' => 
        sl := 'Z';
      WHEN 'W' => 
        sl := 'W';
      WHEN 'L' => 
        sl := 'L';
      WHEN 'H' => 
        sl := 'H';
      WHEN '-' => 
        sl := '-';
      WHEN OTHERS =>
        sl := 'X'; 
    END CASE;
    RETURN sl;
  END to_std_logic;
  
  
  -- converts a string into std_logic_vector
  
  FUNCTION to_std_logic_vector(s: string) RETURN std_logic_vector IS 
  VARIABLE slv: std_logic_vector(s'high-s'low DOWNTO 0);
  VARIABLE k: integer;
  BEGIN
    k := s'high-s'low;
    FOR i IN s'RANGE LOOP
      slv(k) := to_std_logic(s(i));
      k      := k - 1;
    END LOOP;
    RETURN slv;
  END to_std_logic_vector;                                       
  
  
  
  
  
  
  ----------------
  --  file I/O  --
  ----------------
  
  
  
  -- read variable length string from input file
  
  PROCEDURE str_read(FILE in_file: TEXT; 
  res_string: OUT string) IS
  
  VARIABLE l:         line;
  VARIABLE c:         character;
  VARIABLE is_string: boolean;
  
  BEGIN
    
    readline(in_file, l);
    -- clear the contents of the result string
    FOR i IN res_string'RANGE LOOP
      res_string(i) := ' ';
    END LOOP;   
    -- read all characters of the line, up to the length  
    -- of the results string
    FOR i IN res_string'RANGE LOOP
      read(l, c, is_string);
      res_string(i) := c;
      IF NOT is_string THEN -- found end of line
        EXIT;
      END IF;   
    END LOOP; 
    
  END str_read;
  
  
  -- print string to a file
  PROCEDURE print(FILE out_file: TEXT;
  new_string: IN  string) IS
  
  VARIABLE l: line;
  
  BEGIN
    
    write(l, new_string);
    writeline(out_file, l);
    
  END print;
  
  
  -- print string to a file if active is true and start new line
  PROCEDURE print(        active: boolean;
                  FILE out_file : TEXT;
                     new_string : IN  string) IS
  
  VARIABLE l: line;
  
  BEGIN
    IF active THEN    
      write(l, new_string);
      writeline(out_file, l);
    END IF;
  END print;
  
  
  -- print string to a file with current simulation time appended at end
  PROCEDURE printt(FILE out_file: TEXT;
  new_string: IN  string) IS
  
  VARIABLE l: line;
  
  BEGIN
    
    write(l, new_string & " @ ");
    write(l, now);
    writeline(out_file, l);
    
  END printt;
  
  
  -- print character to a file and start new line
  PROCEDURE print(FILE out_file: TEXT;
  char: IN  character) IS
  
  VARIABLE l: line;
  
  BEGIN
    
    write(l, char);
    writeline(out_file, l);
    
  END print;
  
  
  
  -- appends contents of a string to a file until line feed occurs
  -- (LF is considered to be the end of the string)
  
  PROCEDURE str_write(FILE out_file: TEXT; 
  new_string: IN  string) IS
  BEGIN
    
    FOR i IN new_string'RANGE LOOP
      print(out_file, new_string(i));
      IF new_string(i) = LF THEN -- end of string
        EXIT;
      END IF;
    END LOOP;               
    
  END str_write;
  
END PACKAGE BODY txt_util;
 
 
------------------------------------------------------------------------------
-- TYPE CONVERSION UTILITIES
------------------------------------------------------------------------------

-- IEEE library
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;


PACKAGE conv_util IS

  -- function to convert integers to std logic vectors
  FUNCTION int_to_slv (int : integer; slv_width : positive) RETURN std_logic_vector;
  
  -- function to convert std logic vectors to integers
  FUNCTION slv_to_int (slv:std_logic_vector) RETURN integer;


END PACKAGE conv_util;


PACKAGE BODY conv_util IS

  -- function to convert integers to std logic vectors
  -- will convert integers in range -2 to max integer value.
  -- -1 is converted to 'X'
  -- -2 is converted to 'U'
  FUNCTION int_to_slv (int : integer; slv_width : positive) RETURN std_logic_vector IS
    VARIABLE x : std_logic_vector(slv_width-1 DOWNTO 0):=(OTHERS=>'0');
    VARIABLE temp_int : integer := int;
  BEGIN
    CASE temp_int IS
        -- unknown / undefined
      WHEN -1 =>
        FOR i IN x'reverse_range LOOP
          x(i) := 'X';
        END LOOP;     
        -- uninitialized
      WHEN -2 =>
        FOR i IN x'reverse_range LOOP
          x(i) := 'U';
        END LOOP;     
        -- zero
      WHEN 0 =>
        NULL;    
        -- all other possibilities
      WHEN OTHERS =>
        FOR i IN x'reverse_range LOOP
          IF (temp_int MOD 2) = 1 THEN
            x(i) := '1';
          END IF;
          temp_int := temp_int / 2;
        END LOOP;
    END CASE;
    RETURN x;
  END int_to_slv;
  
  
  -- function to convert std logic vectors to integers
  FUNCTION slv_to_int (slv:std_logic_vector) RETURN integer IS
    VARIABLE int : integer:=0;
    VARIABLE wrong_type : boolean:=false;
  
  BEGIN
    ASSERT (slv'high - slv'low + 1) <= 31
    REPORT "Range of std logic vector argument exceeds integer range" 
    SEVERITY failure;
    
    FOR i IN slv'RANGE LOOP
      int := int * 2;
      
      CASE slv(i) IS
        WHEN '1' | 'H' => int := int + 1;
        WHEN '0' | 'L' => NULL;
        WHEN OTHERS    => wrong_type := true;
      END CASE;
      
    END LOOP;
    
    ASSERT NOT wrong_type
    REPORT "slv_to_int converts only 1, 0, H & L" 
    SEVERITY failure;
    
    IF wrong_type THEN
      RETURN 0;
    ELSE
      RETURN int;
    END IF;
    
  END slv_to_int;


END PACKAGE BODY conv_util;

-------------------------------------------------------------------------------
-- END OF FILE : SIM_UTIL.VHD
-------------------------------------------------------------------------------

